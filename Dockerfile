FROM php:7.3-fpm

RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

RUN apt-get update && apt-get install -y cron git-core jq unzip vim zip \
    libjpeg-dev libpng-dev libpq-dev libsqlite3-dev libwebp-dev libzip-dev zlib1g-dev && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-configure zip --with-libzip && \
    docker-php-ext-configure gd --with-png-dir --with-jpeg-dir --with-webp-dir && \
    docker-php-ext-install exif gd mysqli opcache pdo_pgsql pdo_mysql zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

## Install spx extension

# git clone of the project
RUN mkdir -p /usr/src/php/ext \
    && cd /usr/src/php/ext \
    && git clone https://github.com/NoiseByNorthwest/php-spx.git spx \
    && cd spx/

# run the classic extension installation (same one as indicated in the documentation)
RUN cd /usr/src/php/ext/spx && phpize && ./configure
RUN docker-php-ext-install spx

#RUN php artisan october:up && \
#  php artisan plugin:install october.drivers && \
#